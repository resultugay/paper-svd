#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <unistd.h>     //socket headers
#include <netdb.h>      //socket headers
#include <netinet/in.h> //socket headers
#include "mkl.h"

#define BUFFER_SIZE 8

struct thread_data{
   int thread_id;
   int number_of_rows;
   int number_of_columns;
   int size_of_nonzeros;
   double *data;
   double *rows;
   double *cols;
   double *dataTmp;
};

typedef struct thread_data thread_data;

void print_matrix( char* desc, int m, int n, double* a, int lda );
void print_matrix2( char* desc, int m, int n, double* a, int lda );
int main(int argc, char *argv[]) {

   int sockfd, portno, n;
   struct sockaddr_in serv_addr;
   struct hostent *server;

   if (argc < 1) {
      fprintf(stderr,"usage %s hostname port\n", argv[0]);
      exit(0);
   }

   portno = 5000;//atoi(argv[2]);
   

   sockfd = socket(AF_INET, SOCK_STREAM, 0);
   
   if (sockfd < 0) {
      perror("ERROR opening socket");
      exit(1);
   }
	
   server = gethostbyname("127.0.0.1"/*argv[1]*/);
   
   if (server == NULL) {
      fprintf(stderr,"ERROR, no such host\n");
      exit(0);
   }

   bzero((char *) &serv_addr, sizeof(serv_addr));
   serv_addr.sin_family = AF_INET;
   bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
   serv_addr.sin_port = htons(portno);
   

   if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
      perror("ERROR connecting");
      exit(1);
   }

   thread_data data;
   n = read(sockfd,&data.thread_id,sizeof(int)); //Thread_id is taken

   if (n < 0) {
      perror("ERROR writing to socket(thread_id)");
      exit(1);
   }

   n = read(sockfd,&data.number_of_rows,sizeof(int)); //number_of_rows is taken

   if (n < 0) {
      perror("ERROR writing to socket(number_of_rows)");
      exit(1);
   }

   n = read(sockfd,&data.number_of_columns,sizeof(int)); //number_of_columns is taken

   if (n < 0) {
      perror("ERROR writing to socket(number_of_columns)");
      exit(1);
   }

   n = read(sockfd,&data.size_of_nonzeros,sizeof(int)); //size_of_nonzeros is taken

   if (n < 0) {
      perror("ERROR writing to socket(size_of_nonzeros)");
      exit(1);
   }
   //getchar();

   printf("My thread id is %d \n",data.thread_id);
   printf("My row number is %d \n",data.number_of_rows);
   printf("My column number is %d \n",data.number_of_columns);
   printf("size_of_nonzeros is %d \n",data.size_of_nonzeros);


   int number_of_rows = data.number_of_rows;
   int number_of_columns = data.number_of_columns;
   int size_of_nonzeros = data.size_of_nonzeros;

   data.data = malloc(number_of_rows * number_of_columns * sizeof *data.data);

   data.rows = malloc(size_of_nonzeros * sizeof *data.rows);
   data.cols = malloc(size_of_nonzeros * sizeof *data.cols);
   data.dataTmp = malloc(size_of_nonzeros * sizeof *data.dataTmp);


   int i = 0;

   for(i = 0 ; i < size_of_nonzeros ; i = i + (BUFFER_SIZE/sizeof(double))){

      n = read(sockfd,&data.rows[i],BUFFER_SIZE); // rows is received
      if (n < 0) {
        perror("ERROR writing to socket(rows)");
        exit(1);
      }

   }

   for(i = 0 ; i < size_of_nonzeros ; i = i + (BUFFER_SIZE/sizeof(double))){

      n = read(sockfd,&data.cols[i],BUFFER_SIZE); // cols is received
      if (n < 0) {
        perror("ERROR writing to socket(cols)");
        exit(1);
      }

   }

///////////////////////////////////// data is sent here //////////////////////////////////
   for(i = 0 ; i < size_of_nonzeros ; i = i + (BUFFER_SIZE/sizeof(double))){
      n = read(sockfd,&data.dataTmp[i],BUFFER_SIZE); // data is received
      if (n < 0) {
        perror("ERROR writing to socket(cols)");
        exit(1);
      }
      //printf("data is %6.2f\n",data.dataTmp[i]);

   }
///////////////////////////////////// data is sent here //////////////////////////////////



   for(i = 0 ; i < size_of_nonzeros ; i++){
	//printf("%lf %lf\n",data.rows[i],data.cols[i]);
        data.data[(int)data.rows[i]*number_of_columns+(int)data.cols[i]] = data.dataTmp[i];
   }


   /*
   for(i = 0 ; i < data.number_of_rows*data.number_of_columns ; i = i + (BUFFER_SIZE/sizeof(double))){
      n = read(sockfd,&data.data[i],BUFFER_SIZE); // number_of_columns is sent
      if (n < 0) {
        perror("ERROR writing to socket(number_of_columns)");
        exit(1);
      }

   }
   */  
   int  j = 0;

/*
   for(i = 0 ; i <  data.number_of_rows ; i++){
      for(j = 0 ; j < data.number_of_columns ; j++){
          printf("%6.2f ", data.data[i*data.number_of_columns + j]);
      }
          printf("\n");
   }
*/

   int zeros = 0 ; 

   for(i = 0 ; i <  data.number_of_rows ; i++){
      for(j = 0 ; j < data.number_of_columns ; j++){
          if  (data.data[i*data.number_of_columns + j] == 0) 
  	    zeros++;
      }
          if(zeros >=  data.number_of_columns)
	    printf("%d row is zero\n",i);
	  zeros = 0;
   }
   


   /* Locals */
   int m = data.number_of_rows;
   n = data.number_of_columns;
   int lda = m;
   int ldu = m;
   int ldvt = n;
  
   /* singular values */
   double * s;
 
   /* left singular vectors */
   double * u;
 
   /* right singular vectors */
   double * vt;
 
   s = (double *)malloc(sizeof(double)*m);
   for (int i = 0; i < m; i++) {
     s[i] = 0.0;
   }
  
   u = (double *)malloc(sizeof(double)*m*m);
   for (int i = 0; i < m*m; i++) {
     u[i] = 0.0;
   }

   //try not allocating vt if you don't want to generate right singular vectors?
   /*
   vt = (double *)malloc(sizeof(double)*n*n);
	
   for (int i = 0; i < n*n; i++) {
     vt[i] = 0.0;
   }
   */
 
   /* temporary variables*/
   double wkopt;
   double* work;
   int lwork, info;

//transpose is calculated here
   double *transpose = malloc(number_of_rows * number_of_columns * sizeof *data.data);

     for(i=0; i<data.number_of_rows; ++i)
        for(j=0; j<data.number_of_columns; ++j)
        {
            transpose[i+number_of_rows*j] = data.data[i*data.number_of_columns + j];;
        }

     for(i=0; i<data.number_of_rows*data.number_of_columns; ++i)
        data.data[i] = transpose[i];

   free((void *)transpose);
   //print_matrix( "Transpose", number_of_columns, number_of_rows, data.data , number_of_rows );
//transpose is calculated here


   /*http://www.netlib.org/lapack/explore-html/d1/d7e/group__double_g_esing_ga84fdf22a62b12ff364621e4713ce02f2.html#ga84fdf22a62b12ff364621e4713ce02f2*/
   /* query optimal workspace */
   lwork = -1;
   dgesvd( "A", "N", &m, &n, data.data , &lda, s, u, &ldu, vt, &ldvt, &wkopt, &lwork,&info );
   lwork = (int)wkopt;
   work = (double*)malloc( lwork*sizeof(double) );
  
   /* Compute SVD */
   dgesvd( "A", "N", &m, &n, data.data , &lda, s, u, &ldu, vt, &ldvt, work, &lwork,&info );

   /* Check for convergence */
   if( info > 0 ) {
     printf( "The algorithm computing SVD failed to converge.\n" );
     exit( 1 );
   } 
   if( info < 0 ) {
     printf( "The algorithm computing SVD failed to converge.\n" );
     exit( 1 );
   }

	

   n = write(sockfd,&data.thread_id,sizeof(int)); // thread_id is sent

   int u_size = m*m;
   int sigma_size = m;

   n = write(sockfd,&u_size,sizeof(int)); // u_size is sent

   for(i = 0 ; i < u_size ; i = i + (BUFFER_SIZE/sizeof(double))){

      n = write(sockfd,&u[i],BUFFER_SIZE); // u is sent
      if (n < 0) {
        perror("ERROR writing to socket(s)");
        exit(1);
      }

   }

   n = write(sockfd,&sigma_size,sizeof(int)); // sigma_size is sent
   //printf("Sigma size is %d\n",sigma_size);

   int vt_size = number_of_columns;
   n = write(sockfd,&vt_size,sizeof(int)); // vt_size is sent
   //printf("vt_size size is %d\n",vt_size);


   for(i = 0 ; i < sigma_size ; i = i + (BUFFER_SIZE/sizeof(double))){

      n = write(sockfd,&s[i],BUFFER_SIZE); // sigma is sent
      if (n < 0) {
        printf("ERROR writing to socket(sigma) %d \n",i);
        exit(1);
      }

   }



   //print_matrix( "Singular values", 1, m, s, 1 );
   //print_matrix( "Singular values", 1, m, s, 1 );
    /* Print left singular vectors */
   //print_matrix( "Left singular vectors (stored columnwise)", m, m, u, ldu );

 
   // Free workspace
   free( (void*)work );
   free( (void*)s);
   free( (void*)u);
   free(data.data);
   free( (void*)data.rows);
   free( (void*)data.cols);
   //free( (void*)vt);  


}

/* Auxiliary routine: printing a matrix */
void print_matrix( char* desc, int m, int n, double* a, int lda ) {
        int i, j;
        printf( "%s\n", desc );
        for( i = 0; i < m; i++ ) {
                for( j = 0; j < n; j++ ) printf( " %6.2f", a[i+j*lda] );
                printf( "\n" );
        }
}




