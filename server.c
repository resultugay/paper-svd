#include <stdio.h>
#include <stdlib.h>
#include <strings.h> //bzero function

#include <unistd.h>    //socket headers
#include <netdb.h>     //socket headers
#include <netinet/in.h>//socket headers

#include <pthread.h>   //for thread functions
#include <math.h>
#include "mkl.h"
#include "kmeans/kmeans.h"
//#include <time.h>


#define BUFFER_SIZE 8

struct thread_data{
   int thread_id;
   int number_of_rows;
   int number_of_columns;
   double *data;
   double *rows;
   double *cols;
   int size_of_nonzeros;
   int socket_number;
   double *u;
   int u_size;
   int sigma_size;
   int vt_size;
   double *s;
};

typedef struct thread_data thread_data;

void print_matrix2(int row,int col,double **arry);
void print_matrix( char* desc, int m, int n, double* a, int lda );
void *send_and_receive_tf(void *thread_arg);
//quick sort compare function descending.
int comp (const void * elem1, const void * elem2) 
{
    double f = *((double*)elem1);
    double s = *((double*)elem2);
    if (f > s) return -1;
    if (f < s) return 1;
    return 0;
}
int main(int argc, char *argv[]){
	
freopen("test.txt","a",stdout);
printf("\n--------------------------------\n");
srand(time(NULL));
clock_t begin = clock();

int calculate_right = 0;
///////////////////////////////////////////////////////////////  FILE_READ  ///////////////////////////////////////////////////////////////

   //file pointer, gets data from the file given by first command line argument
   FILE *fp;
   fp = fopen(argv[1], "r");
   int total_row_number,total_column_number,i,j,nonzeros;
   //First line indicates number of rows and columns and nonzeros
   fscanf(fp,"%d",&total_row_number);
   printf("Number of rows : %d\n", total_row_number);
   fscanf(fp,"%d",&total_column_number);
   printf("Number of columns : %d\n", total_column_number);
   fscanf(fp,"%d\n",&nonzeros);
   printf("Number of nonzeros : %d\n", nonzeros);

   double ** full_matrix = (double **)malloc(total_row_number * sizeof(double *));
   for (i = 0; i < total_row_number; i++)
       full_matrix[i] = (double *)malloc(total_column_number * sizeof(double));

   for(i = 0; i < total_row_number ; i++)
      for(j = 0; j < total_column_number ; j++)
        full_matrix[i][j] = 0.0;

   //variables contain temporary values from the file when reading 
   int t1,t2;
   double t3;

   //Totally nonzeros is read from the file each one is seperated by a space and each enry by a line
   for(int i = 0 ; i < nonzeros * 3 ; i++){
      fscanf(fp,"%d",&t1);
      fscanf(fp,"%d",&t2);
      fscanf(fp,"%lf\n",&t3);  
      full_matrix[t1-1][t2-1] = t3;
   }
   //print_matrix2(total_row_number,total_column_number,full_matrix); 
 
   int tmp_count_nonzero = 0;
   for(int i = 0 ; i < total_row_number ; i++)
      for(int j = 0 ; j < total_column_number; j++)
 	 if(full_matrix[i][j] != 0)
           tmp_count_nonzero++;
   nonzeros = tmp_count_nonzero;
   printf("Number of total nonzeros : %d\n", nonzeros);
///////////////////////////////////////////////////////////////  FILE_READ  ///////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////  SOCKET  //////////////////////////////////////////////////////////////////

   int number_of_connections = atoi(argv[2]);
   printf("Number of connection is : %d\n", number_of_connections);
   socklen_t clilen;
   int sockfd, newsockfd, portno;
   struct sockaddr_in serv_addr, cli_addr;
   int  n;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
//for reuse port again without waiting more
if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0)
    perror("setsockopt(SO_REUSEADDR) failed");

   if (sockfd < 0) {
      perror("ERROR opening socket");
      exit(1);
    }

   bzero((char *) &serv_addr, sizeof(serv_addr));
   portno = 5000; // port number ,can be changed later on.
   serv_addr.sin_family = AF_INET;
   serv_addr.sin_addr.s_addr = INADDR_ANY;
   serv_addr.sin_port = htons(portno); 
   if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
     perror("ERROR on binding");
     exit(1);
   }
   listen(sockfd,number_of_connections);
   clilen = sizeof(cli_addr);

///////////////////////////////////////////////////////////////  SOCKET  //////////////////////////////////////////////////////////////////


   pthread_t threads[number_of_connections];
   int rc ;
   pthread_attr_t attr;
   pthread_attr_init(&attr);
   //Number of columns to be sent to the each worker is computed here
   int columns_to_sent [number_of_connections] ;
   int size = total_column_number / number_of_connections;
   for(int i = 0 ; i < number_of_connections - 1 ;i++){
      columns_to_sent[i] = size;		
   }
   
   int tmp = total_column_number - (total_column_number / number_of_connections) * (number_of_connections - 1);
   columns_to_sent[number_of_connections - 1] = tmp ;

///////////////////////////////////////////////////////////////  RANK CHECKER  ////////////////////////////////////////////////////////////////////
 int randomChecker = 0;
 int c = 0;
 int km = 0;
 if(randomChecker){
   //local temporary variables

   tmp = 0;
   int tmp_put_one_place = 0;
   for(c = 0 ; c < number_of_connections ; c++){
	
     if(c == 0)
       km = 0;
     else
       km += columns_to_sent[c-1];

	   for(i = 0; i < total_row_number ; i++){
	      for(j = km; j < km + columns_to_sent[c] ; j++){
		   //printf("i : %d j : %d and value %6.2f \n",i,j,full_matrix[i][j]);
		   if(full_matrix[i][j] == 0)
		     tmp++;
	      }
	      if(tmp == columns_to_sent[c]){
	 	   //printf("first %d is \n",i);
   		   //srand(time(0));
 		   int place = rand() % (columns_to_sent[c]);
		   full_matrix[i][place + km] = 1;
		}
		tmp = 0;
	   }
   }
}else{
   
   tmp = 0;
   int tmp_put_one_place = 0;

   int *neighborList = malloc(total_row_number * sizeof(int));
   int *neighborCandList = malloc(total_row_number * sizeof(int));

   for(int p = 0  ; p < total_row_number ; p++)
	neighborList[p] = -1;


   for(c = 0 ; c < number_of_connections; c++){
	
     if(c == 0)
       km = 0;
     else
       km += columns_to_sent[c-1];

	   for(i = 0; i < total_row_number ; i++){
	      for(j = km; j < km + columns_to_sent[c] ; j++){
		   //printf("i : %d j : %d and value %6.2f \n",i,j,full_matrix[i][j]);
		   if(full_matrix[i][j] == 0)
		     tmp++;
	      }
	      if(tmp == columns_to_sent[c]){
		   //printf("%d is \n",i);
		     //fflush(stdout);
  		   int neighborListCount = 0;
  	           for(int p = 0  ; p < total_row_number ; p++){
		     neighborList[p] = -1; 
		     neighborCandList[p] = -1;
		}

		   for(int u = 0 ; u < total_column_number ; u++){    

			if(full_matrix[i][u] != 0){
			  
			  for(int t = 0 ; t < total_row_number ; t++){
 			     if(full_matrix[t][u] != 0 && t != i)
				neighborCandList[t] = t;
			  }  
		       
			}
		   }

		   for(int h  = 0 ; h < total_row_number ; h++)
			if(neighborCandList[h] != -1)
			   neighborList[neighborListCount++] = neighborCandList[h];
			
		   //printf("neighcount %d\n",neighborListCount);
		   //fflush(stdout);
		   if(neighborListCount == 0 ){
   		      //srand(time(NULL));
 		      int place = rand() % (columns_to_sent[c]);
		      full_matrix[i][place + km] = 1;
		      //printf("km0 is %d and col %d and pla %d\n",km,columns_to_sent[c],place);
		   }else if(neighborListCount == 1){
			full_matrix[i][km + neighborList[0]] = 1;
   		        //srand(time(NULL));
 		        int sec_place = rand() % (columns_to_sent[c]);
		        full_matrix[i][sec_place + km] = 1;
			//printf("km1 is %d and sec %d and col %d\n",km,sec_place,columns_to_sent[c]);

		   }else{
   		     //srand(time(NULL));
 		     int place = rand() % (neighborListCount);
		     //printf("place is %d\n",neighborList[place]);
		     full_matrix[i][ km + neighborList[place]] = 1;	

 		     int sec_place = rand() % (columns_to_sent[c]);
		     full_matrix[i][sec_place + km] = 1;
		     //printf("km2 is %d and col%d and plac %d %d \n",km,columns_to_sent[c],sec_place,rand());
		     //fflush(stdout);
		  }
	       }
		tmp = 0;
	   }
   }
 


}
   //print_matrix2(total_row_number,total_column_number,full_matrix); 
///////////////////////////////////////////////////////////////  RANK CHECKER  ////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////  DEGREE CALCULATION ///////////////////////////////////////////////////////////////

   //Row Degree Calculation

   double * d1 = (double *) malloc(total_row_number * sizeof(double) );

   for(int i = 0 ; i < total_row_number; i++)
     d1[i] = 0.0;

   for(int i = 0 ; i < total_row_number ; i++){
      int degree = 0;      
      for(int j = 0 ; j < total_column_number ; j++){
	 if(full_matrix[i][j] != 0 )
	    degree++;	
      }
     if(degree != 0.0)
      d1[i] = 1/sqrt(degree);
     else
      d1[i] = 0.0; 
   }

   //quick sort to see what is the degree of each vertex
   //qsort (d1,total_row_number, sizeof(*d1), comp);
   //for (int i = 0 ; i < total_row_number ; i++)
   //     printf ("%d %6.2lf \n",i, d1[i]);
   
   //for(int i = 0 ; i < total_row_number; i++)
   //   printf("%d is %lf\n",i,d1[i]);
   
   //Row Degree Calculation

   //Column Degree Calculation

   double * d2 = (double *) malloc(total_column_number * sizeof(double) );

   for(int i = 0 ; i < total_column_number; i++)
     d2[i] = 0.0;

   for(int i = 0 ; i < total_column_number ; i++){
      int degree = 0;      
      for(int j = 0 ; j < total_row_number ; j++){
	 if(full_matrix[j][i] != 0 )
	    degree++;	
      }
     if(degree != 0.0)
      d2[i] = 1/sqrt(degree);
     else
      d2[i] = 0.0; 
   }


   //quick sort to see what is the degree of each vertex
   /*
   qsort (d2,total_column_number, sizeof(*d2), comp);
   for (int i = 0 ; i < total_column_number ; i++)
	if(d2[i] <= 10 ){
           printf ("First %d vertex's degree is %6.2lf, others less than %6.2lf \n",i, d2[i],d2[i]);
  	   break;
	}
   */
   //for(int i = 0 ; i < total_column_number; i++)
   //   printf("%d is %lf\n",i,d2[i]);
   
   //Column Degree Calculation

///////////////////////////////////////////////////////////////  DEGREE CALCULATION ///////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////// MATRIX MULTIPLICATION /////////////////////////////////////////////////////////////

   for(int i = 0 ; i < total_row_number ; i++)
      for(int j = 0 ; j < total_column_number ; j++)
	 full_matrix[i][j] *= d1[i]; 

   for(int i = 0 ; i < total_row_number ; i++)
      for(int j = 0 ; j < total_column_number ; j++)
	 full_matrix[i][j] *= d2[j]; 

   //print_matrix2(total_row_number,total_column_number,full_matrix); 

/////////////////////////////////////////////////////////////// MATRIX MULTIPLICATION /////////////////////////////////////////////////////////////

   int tmp_connections = 0;
   thread_data t[number_of_connections];

   for(i = 0 ; i < number_of_connections; i++)
       t[i].data = malloc(total_row_number*columns_to_sent[i] * sizeof *t[i].data);

   //2D array is mapped to 1D array

   for(c = 0 ; c < number_of_connections ; c++){
     t[c].size_of_nonzeros = 0;
     if(c == 0)
       km = 0;
     else
       km += columns_to_sent[c-1];
   
     for(j = 0 ; j < columns_to_sent[c] ; j++){
        for(i = 0 ; i < total_row_number ; i++){
           t[c].data[i*columns_to_sent[c] + j] = full_matrix[i][km+j];  
            
	   if(full_matrix[i][km+j] != 0)
	     t[c].size_of_nonzeros++;	    
	}
     }
     //printf("total zeros is %d\n", t[c].size_of_nonzeros);
     //printf("from %d to %d \n", km, km+columns_to_sent[c]);malloc(t[k].size_of_nonzeros * sizeof *t[k].cols);
   }

   int k = 0;
   for(k = 0 ; k < number_of_connections ; k++){
       t[k].rows = malloc(t[k].size_of_nonzeros * sizeof *t[k].rows);
       t[k].cols = malloc(t[k].size_of_nonzeros * sizeof *t[k].cols);
   }
   
   for(c = 0 ; c < number_of_connections ; c++){
     int tmp = 0;
     if(c == 0)
       km = 0;
     else
       km += columns_to_sent[c-1];

     for(i = 0 ; i < total_row_number ; i++){
        for(j = 0 ; j < columns_to_sent[c] ; j++){
	   if(full_matrix[i][km+j] != 0){
	     t[c].rows[tmp] = i;
	     t[c].cols[tmp++] = j;
	   }	    
	}
     }
	//printf("temp is %d\n",tmp);
   }
   printf("Now Clients can connect\n");

   /*
   for(c = 0 ; c < number_of_connections ; c++){
     for(i = 0 ; i < t[c].size_of_nonzeros ; i++){
	printf("c %d i %d and row %lf and col %lf \n",c,i,t[c].rows[i],t[c].cols[i]);

     }
   }
   */
   //clients are being waited
   while(tmp_connections < number_of_connections){
      newsockfd = accept(sockfd, (struct sockaddr*)&cli_addr, &clilen);
      t[tmp_connections].thread_id = tmp_connections;
      t[tmp_connections].number_of_rows = total_row_number;	
      t[tmp_connections].number_of_columns = columns_to_sent[tmp_connections];
      t[tmp_connections].socket_number = newsockfd;
      rc = pthread_create(&threads[tmp_connections], &attr, send_and_receive_tf, (void *) &t[tmp_connections]);
      tmp_connections = tmp_connections + 1;
      if (newsockfd < 0) {
        perror("ERROR on accept");
        exit(1);
      }
    }

   //thread joining	
   void *status;
   for(i = 0; i < number_of_connections ; i++) {
       rc = pthread_join(threads[i], &status);
       if (rc) {
          printf("ERROR; return code from pthread_join() is %d\n", rc);
          exit(-1);
          }
	//printf("Main: completed join with thread %d having a status %ld\n",i,(long)status);
       }

   double *proxy_matrix  = malloc(total_row_number*total_row_number*number_of_connections* sizeof(double));
   for (i = 0; i < total_row_number*total_row_number*number_of_connections; i++) {
     proxy_matrix[i] = 0.0;
   }
   

   for(c = 0 ; c < number_of_connections ; c++){ 
      for(i = 0 ; i < t[c].u_size ; i++){
         for(j = 0 ; j < t[c].u_size ; j++){
            //printf("c is %d %lf %lf \n",c, t[c].u[i*t[c].u_size + j], t[c].s[j]);
            double tmp = 0.0;
            for(k = 0 ; k < t[c].u_size ; k++){
               if(j == k)
	       tmp += t[c].u[i+t[c].u_size*k]*t[c].s[k];
            }
	    proxy_matrix[i*t[c].u_size*number_of_connections+j + c*t[c].u_size] = tmp;
	    //printf("%d tmp %6.2f\n",i+t[c].u_size*number_of_connections*j + c*t[c].u_size,tmp);
         }
      }
   }

   //transpose is calculated here
   double *transpose = (double *)malloc(total_row_number*total_row_number*number_of_connections* sizeof(double));

     for(i=0; i<total_row_number; ++i)
        for(j=0; j<total_row_number*number_of_connections; ++j)
        {
            transpose[i+total_row_number*j] = proxy_matrix[i*total_row_number*number_of_connections + j];;
        }

     for(i=0; i<total_row_number*total_row_number*number_of_connections; ++i)
        proxy_matrix[i] = transpose[i];

   free((void *)transpose);
   //transpose is calculated here

   //print_matrix("Prox",total_row_number, total_row_number*number_of_connections , proxy_matrix,total_row_number);

////////////////////////////////////////// SVD OF PROXY MATRIX IS CALCULATED HERE ////////////////////////////////////////////////////////////

   /* Locals */
   int m = total_row_number;
   n =  total_row_number*number_of_connections;
   int lda = m;
   int ldu = m;
   int ldvt = n;
  
   /* singular values */
   double * s;
 
   /* left singular vectors */
   double * u;
 
   /* right singular vectors */
   double * vt;
 
   s = (double *)malloc(sizeof(double)*m);
   for (int i = 0; i < m; i++) {
     s[i] = 0.0;
   }
  
   u = (double *)malloc(sizeof(double)*m*m);
   for (int i = 0; i < m*m; i++) {
     u[i] = 0.0;
   }

   //try not allocating vt if you don't want to generate right singular vectors?
   /*
   vt = (double *)malloc(sizeof(double)*n*n);
	
   for (int i = 0; i < n*n; i++) {
     vt[i] = 0.0;
   }
   */
 
   /* temporary variables*/
   double wkopt;
   double* work;
   int lwork, info;

   printf("started calculation svd of proxy\n");

   /*http://www.netlib.org/lapack/explore-html/d1/d7e/group__double_g_esing_ga84fdf22a62b12ff364621e4713ce02f2.html#ga84fdf22a62b12ff364621e4713ce02f2*/
   /* query optimal workspace */
   lwork = -1;
   dgesvd( "A", "N", &m, &n, proxy_matrix , &lda, s, u, &ldu, vt, &ldvt, &wkopt, &lwork,&info );
   lwork = (int)wkopt;
   work = (double*)malloc( lwork*sizeof(double) );
  
   /* Compute SVD */
   dgesvd( "A", "N", &m, &n, proxy_matrix , &lda, s, u, &ldu, vt, &ldvt, work, &lwork,&info );

   /* Check for convergence */
   if( info > 0 ) {
     printf( "The algorithm computing SVD failed to converge.\n" );
     exit( 1 );
   } 
   if( info < 0 ) {
     printf( "The algorithm computing SVD failed to converge.\n" );
     exit( 1 );
   }
   printf("finished calculation svd of proxy\n");
	
   //print_matrix( "Singular values of proxy matrix is", 1, 3, s, 1 );
   /* Print left singular vectors */
   //print_matrix( "Left singular vectors (stored columnwise)", m, m, u, ldu );

////////////////////////////////////////// SVD OF PROXY MATRIX IS CALCULATED HERE ////////////////////////////////////////////////////////////

   double ** full_matrix_transpose = (double **)malloc(total_column_number * sizeof(double *));
   for (i = 0; i < total_column_number; i++)
       full_matrix_transpose[i] = (double *)malloc(total_row_number * sizeof(double));

   for(i = 0; i < total_row_number ; i++)
      for(j = 0; j < total_column_number ; j++)
        full_matrix_transpose[j][i] = full_matrix[i][j];

   
   //memory free for 2d array
   for (i = 0; i < total_row_number; i++)
      free( (void*)full_matrix[i]);

   free( (void*)full_matrix);

  /*  printf("Transpose of full_matrix\n");
   for(i = 0; i < total_column_number ; i++){
      for(j = 0; j < total_row_number ; j++)
           printf("%6.1f ",full_matrix_transpose[i][j]);
      printf("\n");
   }*/


   /*right singular vectors are calculated here*/
  
   double ** right_singular_vector = (double **)malloc(total_column_number * sizeof(double *));
   for (i = 0; i < total_column_number; i++)
       right_singular_vector[i] = (double *)malloc(total_row_number * sizeof(double));

   for(i = 0; i < total_column_number ; i++)
      for(j = 0; j < total_row_number ; j++)
        right_singular_vector[i][j] = 0.0;
 	

   for(int c = 0 ; c < number_of_connections ; c++){
      //printf("c : %d\n",t[c].vt_size);
      for(i = 0 ; i < t[c].vt_size ; i++){
         for(j = 0 ; j < total_row_number; j++ ){
            double tmp2 = 0.0;	    
            for(int k = 0 ; k < total_row_number ; k++ ){
     	       tmp2 += full_matrix_transpose[i + c*(t[c-1].vt_size) ][k]*u[j*total_row_number+k];
	    }
	    //printf("i %d j %d \n",i + c* (t[c-1].vt_size),j  );
	    right_singular_vector[i + c* (t[c-1].vt_size) ][j] = tmp2/s[j];
         }
      }
   }


   /*

   for(i = 0; i < total_column_number ; i++){
      for(j = 0; j < total_row_number ; j++){
        printf("%6.2f ",right_singular_vector[i][j] );}
	printf("\n");
    }
 	
   */
   double *right_singular_vectors = (double *)malloc(total_row_number*total_column_number*sizeof(double ));
   for (i = 0; i < total_row_number*total_column_number; i++) 
     right_singular_vectors[i] = 0.0;

   
   for(i = 0 ; i < total_column_number ; i++)
      for(j = 0 ; j < total_row_number; j++)
	 right_singular_vectors[i+j*total_column_number] = right_singular_vector[i][j];
	 //right_singular_vectors[i*total_row_number+j] = right_singular_vector[i][j];

   //memory free for 2d right_singular_vector
   for (i = 0; i < total_column_number; i++)
       free( (void*)right_singular_vector[i]);

   free( (void*)right_singular_vector);

   //print_matrix( "Right singular vectors (stored columnwise)", total_column_number, total_row_number, right_singular_vectors, total_column_number );

////////////////////////////////////////////////// CHECKING RESULTS //////////////////////////////////////////////////

   

   double *da = (double *)malloc(total_row_number*total_column_number*sizeof(double ));
   for (i = 0; i < total_row_number*total_column_number; i++) 
     da[i] = 0.0;

   
   for(i = 0 ; i < total_row_number ; i++)
      for(j = 0 ; j < total_column_number; j++)
	 da[i+total_row_number*j] = full_matrix_transpose[j][i];


   //free( (void*)full_matrix_transpose);
   m = total_row_number;
   n =  total_column_number;
   lda = m;
   ldu = m;
   ldvt = n;

   double * s1;

   double * u1;
 
   double * vt1;
 
   s1 = (double *)malloc(sizeof(double)*m);
   for (int i = 0; i < m; i++) {
     s1[i] = 0.0;
   }
  
   u1 = (double *)malloc(sizeof(double)*m*m);
   for (int i = 0; i < m*m; i++) {
     u1[i] = 0.0;
   }

   //try not allocating vt if you don't want to generate right singular vectors?
     if(calculate_right){
	   vt1 = (double *)malloc(sizeof(double)*n*n);
	
	   for (int i = 0; i < n*n; i++) {
	     vt1[i] = 0.0;
	   }
	   

	   printf("started calculation svd of real matrix \n");

	   lwork = -1;
	   

	   dgesvd( "A", "A", &m, &n, da , &lda, s1, u1, &ldu, vt1, &ldvt, &wkopt, &lwork,&info );
	   lwork = (int)wkopt;
	   work = (double*)malloc( lwork*sizeof(double) );
	  
	   dgesvd( "A", "A", &m, &n, da , &lda, s1, u1, &ldu, vt1, &ldvt, work, &lwork,&info );
}
  else {

	   printf("started calculation svd of real matrix \n");

	   lwork = -1;
	   

   dgesvd( "A", "N", &m, &n, da , &lda, s1, u1, &ldu, vt1, &ldvt, &wkopt, &lwork,&info );
   lwork = (int)wkopt;
   work = (double*)malloc( lwork*sizeof(double) );
  
   dgesvd( "A", "N", &m, &n, da , &lda, s1, u1, &ldu, vt1, &ldvt, work, &lwork,&info );

}

   if( info > 0 ) {
     printf( "The algorithm computing SVD failed to converge.\n" );
     exit( 1 );
   } 
   if( info < 0 ) {
     printf( "The algorithm computing SVD failed to converge.\n" );
     exit( 1 );
   }
   printf("finished calculation svd of real matrix \n");

 
   //print_matrix( "Singular values of real values", 1, 5, s1, 1 );
   //print_matrix( "Left singular vectors (stored columnwise)", m, m, u1, ldu );
   //print_matrix( "Left singular vectors (stored columnwise)", m, m, u, ldu );
   //print_matrix( "Right singular vectors (stored rowwise)", n, n, vt1, ldvt );

   double diff_singular_values = 0 ;
   //printf("m is %d and n is %d \n" , m,n);
   for(i = 0 ; i < m ; i++){
      //printf("s1 %lf s %lf fark %lf \n",s1[i],s[i],s1[i]-s[i]);
      diff_singular_values += fabs(s1[i])-fabs(s[i]);
   }
   printf("Difference between singular values is %e\n",diff_singular_values);
   fflush(stdout);

   double diff_left_singular_vectors = 0 ;
   double diff_right_singular_vectors = 0 ;
   for(i = 0 ; i < m*m ; i++){
      diff_left_singular_vectors += fabs(u1[i])-fabs(u[i]);
   }
   printf("Difference between left singular vectors is %e\n",diff_left_singular_vectors);
   fflush(stdout);

   clock_t end = clock();
   double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
   printf("time_spent is %6.6lf\n",time_spent);

  if(calculate_right){
	  for(i = 0 ; i < m*n ; i++){
	      diff_right_singular_vectors += fabs(right_singular_vectors[i])-fabs(vt1[i]);
	   }

	   printf("Difference between right singular vectors is %e\n",diff_right_singular_vectors);

  }
////////////////////////////////////////////////// CHECKING RESULTS //////////////////////////////////////////////////
    

   //don't need full_matrix_transpose anymore.
   for (i = 0; i < total_column_number; i++)
      free( (void*)full_matrix_transpose[i]);

   free( (void*)full_matrix_transpose);



   int number_of_cluster =  atoi(argv[3]);;
   int number_of_dimension = (int)ceil(log2(number_of_cluster));
   printf("Number of dimension of data to be clustered %d is \n",number_of_dimension);
   double *centers = (double *)malloc(number_of_cluster*number_of_dimension*sizeof(double));
int *output ;

  if(calculate_right){
   output = (int *)malloc((total_row_number+total_column_number)*sizeof(int));

   for(i = 0 ; i < total_row_number+total_column_number ; i++)
      output[i] = 0;

}else{
   output = (int *)malloc((total_row_number)*sizeof(int));

   for(i = 0 ; i < total_row_number ; i++)
      output[i] = 0;
}
int whole_data_size = 0;
  if(calculate_right){
   whole_data_size = (total_row_number+total_column_number)*number_of_dimension;
  }else
	{
	   whole_data_size = (total_row_number)*number_of_dimension;
	}



   double *whole_data = (double *)malloc(whole_data_size*sizeof(double));


   for(int i = 0 ; i < whole_data_size ; i++){
      whole_data[i] = 0.0;
   }  


   for(int d = 0 ; d < number_of_dimension ; d++){
      for(int i = 0 ; i < total_row_number ; i++){
         whole_data[i + (total_row_number+total_column_number)*d] = u[i+total_row_number+d*total_row_number]*d1[i];
      }
   }


  if(calculate_right){
	   for(int d = 0 ; d < number_of_dimension ; d++){
	      for(i = 0 ; i < total_column_number; i++){
		 //right_singular_vectors[i+j*total_column_number] = right_singular_vector[i][j];
	   	 whole_data[(i+total_row_number) + (total_row_number+total_column_number)*d ] = right_singular_vectors[i+total_column_number + total_column_number*d]*d2[i];
	      }
	   }
}
   /*
   for(int i = 0 ; i < whole_data_size; i++){
      printf("%6.2f\n",whole_data[i]);
   }
   */

   /*
   for(int i = 0 ; i < total_row_number ; i++){
      printf("%6.2f\n",u[i]);
   }

   for(int i = 0 ; i < total_row_number ; i++){
      printf("%6.2f\n",right_singular_vectors[i]);
   }
   */

   for(i = 0 ; i < number_of_cluster*number_of_dimension ; i++){
      centers[i] = u[i];
      //printf("%6.2f\n",(double)rand() / (double)RAND_MAX);           
   }


   /*
   void kmeans(
            int  dim,		                     // dimension of data 
            double *X,                        // pointer to data
            int   n,                         // number of elements            
            int   k,                         // number of clusters
            double *cluster_centroid,         // initial cluster centroids
            int   *cluster_assignment_final  // output
           );
   */
  if(calculate_right){
   kmeans(number_of_dimension,whole_data,total_row_number+total_column_number,number_of_cluster,centers,output);
  }else{
   kmeans(number_of_dimension,whole_data,total_row_number,number_of_cluster,centers,output);
   }

   for(int i = 0 ; i < total_row_number ; i++){
      //printf("i : %d cluster :  %d\n", i, output[i]);
   }

   //free memory 
   free( (void*)s);
   free( (void*)u);
   free( (void*)proxy_matrix);
   free( (void*)centers);
   free( (void*)output);
   free( (void*)whole_data);
   for(i = 0 ; i < number_of_connections; i++){
      free( (void*)t[i].data);
      free( (void*)t[i].rows);
      free( (void*)t[i].cols);
      free( (void*)t[i].u);
      free( (void*)t[i].s);
   }
   //pthread_exit(NULL);
   return 0;
}



void *send_and_receive_tf(void *thread_arg){
   thread_data *arg;
   arg = (thread_data *) thread_arg;
   int thread_id = arg->thread_id;  
   int rows = arg->number_of_rows;
   int cols = arg->number_of_columns;
   int size_of_nonzeros = arg->size_of_nonzeros;

   int i,j = 0;

  /* for(i = 0 ; i < rows ; i++){
      for(j = 0 ; j < cols ; j++){
          printf("%lf ", arg->data[i*cols + j]);
      }
          printf("\n");
   }*/

   int socket_number = arg->socket_number;
   int n = write(socket_number,&arg->thread_id,sizeof(int)); // thread_id is sent
   if (n < 0) {
      perror("ERROR writing to socket(thread_id)");
      exit(1);
   }
   n = write(socket_number,&arg->number_of_rows,sizeof(int)); // number_of_rows is sent
   if (n < 0) {
      perror("ERROR writing to socket(number_of_rows)");
      exit(1);
   } 

   n = write(socket_number,&arg->number_of_columns,sizeof(int)); // number_of_columns is sent
   if (n < 0) {
      perror("ERROR writing to socket(number_of_columns)");
      exit(1);
   }

   n = write(socket_number,&arg->size_of_nonzeros,sizeof(int)); // size_of_nonzeros is sent
   if (n < 0) {
      perror("ERROR writing to socket(size_of_nonzeros)");
      exit(1);
   }

  
   for(i = 0 ; i < size_of_nonzeros ; i = i + (BUFFER_SIZE/sizeof(double))){
      //printf("rows : %d %lf \n",i,arg->rows[i]);
      n = write(socket_number,&arg->rows[i],BUFFER_SIZE); // rows is sent
      if (n < 0) {
        perror("ERROR writing to socket(rows)");
        exit(1); 
      }

   }

   for(i = 0 ; i < size_of_nonzeros ; i = i + (BUFFER_SIZE/sizeof(double))){
      //printf("cols : %d %lf \n",i,arg->cols[i]);
      n = write(socket_number,&arg->cols[i],BUFFER_SIZE); // cols is sent
      if (n < 0) {
        perror("ERROR writing to socket(cols)");
        exit(1);
      }

   }

///////////////////////////////////// data is sent here //////////////////////////////////
   for(i = 0 ; i < size_of_nonzeros ; i = i + (BUFFER_SIZE/sizeof(double))){
      n = write(socket_number,&arg->data[(int)arg->rows[i]*cols + (int)arg->cols[i]],BUFFER_SIZE); // cols is sent
      if (n < 0) {
        perror("ERROR writing to socket(cols)");
        exit(1);
      }
      //printf("data is at %6.1f %6.1f %6.2f\n",arg->rows[i],arg->cols[i],arg->data[(int)arg->rows[i]*cols + (int)arg->cols[i]]);

   }
///////////////////////////////////// data is sent here //////////////////////////////////
/*
   for(i = 0 ; i < rows*cols ; i = i + (BUFFER_SIZE/sizeof(double))){
      n = write(socket_number,&arg->data[i],BUFFER_SIZE); // number_of_columns is sent
      if (n < 0) {
        perror("ERROR writing to socket(number_of_columns)");
        exit(1);
      }

   }
*/  
   n = read(socket_number,&thread_id,sizeof(int)); //thread_id is taken
   //printf("Worker %d is back \n",thread_id);

   if (n < 0) {
      perror("ERROR writing to socket(number_of_columns)");
      exit(1);
   }

   /* singular values */
   int u_size;
   n = read(socket_number,&u_size,sizeof(int)); //u_size is taken
   arg->u_size = sqrt(u_size);
   if (n < 0) {
      perror("ERROR writing to socket(u_size)");
      exit(1);
   }
   //printf("u_size %d is from worker %d \n",u_size,thread_id);


   //double * s;

   arg->u = (double *)malloc(sizeof(double)*u_size);
   for (i = 0; i < u_size; i++) {
     arg->u[i] = 0.0;
   }

   for(i = 0 ; i < u_size ; i = i + (BUFFER_SIZE/sizeof(double))){

      n = read(socket_number,&arg->u[i],BUFFER_SIZE); // u is taken
      if (n < 0) {
        perror("ERROR writing to socket(rows)");
        exit(1);
      }

   }   


   /* left singular vectors */

   int sigma_size;
   n = read(socket_number,&sigma_size,sizeof(int)); //sigma_size is taken
   arg->sigma_size = sigma_size;
   if (n < 0) {
      perror("ERROR writing to socket(sigma_size)");
      exit(1);
   }

   //printf("sigma_size %d is from worker %d \n",sigma_size,thread_id);


   int vt_size;
   n = read(socket_number,&vt_size,sizeof(int)); //vt_size is taken
   arg->vt_size = vt_size;
   if (n < 0) {
      perror("ERROR writing to socket(sigma_size)");
      exit(1);
   }

  //printf("vt_size %d is from worker %d \n",vt_size,thread_id);

   //double * u;

   arg->s = (double *)malloc(sizeof(double)*sigma_size);
   for (i = 0; i < sigma_size; i++) {
     arg->s[i] = 0.0;
   }

   for(i = 0 ; i < sigma_size ; i = i + (BUFFER_SIZE/sizeof(double))){

      n = read(socket_number,&arg->s[i],BUFFER_SIZE); // sigma is taken
      if (n < 0) {
        perror("ERROR writing to socket(rows)");
        exit(1);
      }

   }   

   //print_matrix( "Singular values", 1, m, s, 1 );
   //print_matrix( "Singular values", 1, sigma_size, arg->s, 1 );
   /* Print left singular vectors */
   //print_matrix( "Left singular vectors (stored columnwise)", sigma_size, sigma_size, arg->u, sigma_size );

   //print_matrix( "Singular values", 1, u_size, s, 1 );
   //print_matrix( "Singular values", 1, 2, arg->s, 1 );
   //free( (void*)s);
   //free( (void*)u);

}

/* Auxiliary routine: printing a matrix */
void print_matrix( char* desc, int m, int n, double* a, int lda ) {
        int i, j;
        printf( "\n%s\n", desc );
        for( i = 0; i < m; i++ ) {
                for( j = 0; j < n; j++ ) printf( " %6.2f", a[i+j*lda] );
                printf( "\n" );
        }
                printf( "\n" );
}



void print_matrix2(int row,int col,double **arry){
   printf("Matrix is : \n");
   int i,j;
   for(i = 0; i < row ; i++){
      for(j = 0; j < col ; j++){
        printf("%6.2f ", arry[i][j]);
      }
	printf("\n");
   }
}
